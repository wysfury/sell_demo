package cn.xiangxu.demo.sell_demo.util;

import cn.xiangxu.demo.sell_demo.entity.entityVO.ResultVO;

/**
 * @author 王杨帅
 * @create 2018-04-22 14:57
 * @desc 响应结果封装
 **/
public class ResultUtil {

    public static ResultVO success() {
        return success(null);
    }

    public static ResultVO success(Object data) {
        Integer code = 0;
        String message = "success";

        ResultVO resultVO = new ResultVO();
        resultVO.setCode(code);
        resultVO.setMsg(message);
        resultVO.setData(data);

        return resultVO;
    }

    public static ResultVO error() {
        Integer code = -1;
        String message = "error";
        Object data = null;
        return new ResultVO(code, message, data);
    }

    public static ResultVO error(Integer code, String message) {
        Object data = null;
        return new ResultVO(code, message, data);
    }
}

