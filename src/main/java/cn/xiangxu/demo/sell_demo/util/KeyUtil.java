package cn.xiangxu.demo.sell_demo.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Select;

import java.util.Random;

/**
 * @author 王杨帅
 * @create 2018-04-23 8:37
 * @desc 主键生成器
 **/
@Slf4j
public class KeyUtil {

    /**
     * 利用系统当前毫秒数和六位随机数拼接成的字符串作为自定义的唯一主键
     * 技巧01：产生主键时必须考虑高并发的问题
     * @return
     */
    public static  synchronized  String genUniqueKey() {
        Random random = new Random();
        Integer number = random.nextInt(900000) + 100000;
        return System.currentTimeMillis() + String.valueOf(number);
    }

}

