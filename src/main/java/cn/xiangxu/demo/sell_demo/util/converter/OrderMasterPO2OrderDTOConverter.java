package cn.xiangxu.demo.sell_demo.util.converter;

import cn.xiangxu.demo.sell_demo.entity.entityDTO.OrderDTO;
import cn.xiangxu.demo.sell_demo.entity.entityPO.OrderMasterPO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 将OrderMasterPO转化成OrderDTO
 */
public class OrderMasterPO2OrderDTOConverter {
    public static OrderDTO convert(OrderMasterPO orderMaster) {
        OrderDTO orderDTO = new OrderDTO();
        BeanUtils.copyProperties(orderMaster, orderDTO);
        return orderDTO;
    }

    public static List<OrderDTO> convert(List<OrderMasterPO> orderMasters) {
        return orderMasters.stream()
                .map(e -> convert(e))
                .collect(Collectors.toList());
    }
}
