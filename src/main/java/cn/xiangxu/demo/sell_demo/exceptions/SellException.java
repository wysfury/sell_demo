package cn.xiangxu.demo.sell_demo.exceptions;

import cn.xiangxu.demo.sell_demo.enums.ResultEnum;
import lombok.Data;

/**
 * @author 王杨帅
 * @create 2018-04-21 16:04
 * @desc 自定义订餐系统异常
 **/
@Data
public class SellException extends  RuntimeException {
    private Integer code;

    public SellException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public SellException(ResultEnum resultEnum) {
        super(resultEnum.getMessge());
        this.code = code;
    }
}

