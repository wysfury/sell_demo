package cn.xiangxu.demo.sell_demo.entity.entityVO;

import lombok.Data;

/**
 * @author 王杨帅
 * @create 2018-04-22 14:43
 * @desc 响应实体类
 **/
@Data
public class ResultVO<T> {
    private Integer code;
    private String msg;
    private T data;

    public ResultVO() {
    }

    public ResultVO(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}

