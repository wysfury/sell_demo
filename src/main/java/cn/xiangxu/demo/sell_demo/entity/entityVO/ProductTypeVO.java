package cn.xiangxu.demo.sell_demo.entity.entityVO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 王杨帅
 * @create 2018-04-22 14:45
 * @desc 商品类型实响应实体类
 **/
@Data
public class ProductTypeVO {
    /** 商品烈性名称 */
    private String name;

    /** 商品类型编号 */
    private Integer type;

    /** 商品信息列表 */
    @JsonProperty("foods")
    private List<ProductInfoVO> productInfoVOList;
}

