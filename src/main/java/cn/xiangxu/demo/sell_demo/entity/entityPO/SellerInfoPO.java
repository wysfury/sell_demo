package cn.xiangxu.demo.sell_demo.entity.entityPO;

import com.baomidou.mybatisplus.annotations.TableField;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 王杨帅
 * @create 2018-04-21 13:28
 * @desc 买家信息实体类
 **/
@Entity
@Table(name = "seller_info")
@Data
public class SellerInfoPO {
    @Id
    private String id;
    private String username;
    private String password;
    /**
     * 微信openid
     */
    private String openid;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;
}

