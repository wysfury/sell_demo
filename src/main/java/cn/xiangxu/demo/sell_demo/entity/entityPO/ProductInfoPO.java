package cn.xiangxu.demo.sell_demo.entity.entityPO;

import cn.xiangxu.demo.sell_demo.enums.ProductStatusEnum;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 王杨帅
 * @create 2018-04-21 13:25
 * @desc 商品信息实体类
 **/
@Data
@Table(name = "product_info")
@Entity
@DynamicUpdate
public class ProductInfoPO {
    /**
     * 商品ID
     */
    @Id
    @Column(name = "product_id")
    private String productId;
    /**
     * 商品名称
     */
    @Column(name = "product_name")
    private String productName;
    /**
     * 单价
     */
    @Column(name = "product_price")
    private BigDecimal productPrice;
    /**
     * 库存
     */
    @Column(name = "product_stock")
    private Integer productStock;
    /**
     * 描述
     */
    @Column(name = "product_description")
    private String productDescription;
    /**
     * 小图
     */
    @Column(name = "product_icon")
    private String productIcon;
    /**
     * 商品状态,0正常1下架
     */
    @Column(name = "product_status")
    private Integer productStatus = ProductStatusEnum.ON_SALE.getCode();
    /**
     * 类目编号
     */
    @Column(name = "category_type")
    private Integer categoryType;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;
}

