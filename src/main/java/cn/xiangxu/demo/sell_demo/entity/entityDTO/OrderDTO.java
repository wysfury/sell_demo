package cn.xiangxu.demo.sell_demo.entity.entityDTO;

import cn.xiangxu.demo.sell_demo.entity.entityPO.OrderDetailPO;
import cn.xiangxu.demo.sell_demo.util.serializer.Date2Long;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 商品订单和订单详情组合成的数据传输对象
 */
@Data
//@JsonInclude(JsonInclude.Include.NON_NULL) // 数据为null时就不返回
public class OrderDTO {
    /** 订单ID */
    private String orderId;
    /** 买家名字 */
    private String buyerName;
    /** 买家电话 */
    private String buyerPhone;
    /** 买家地址 */
    private String buyerAddress;
    /** 买家微信OpenId */
    private String buyerOpenid;
    /** 订单金额 */
    private BigDecimal orderAmount;
    /** 订单状态，默认为0：新下单 */
    private Integer orderStatus;
    /** 支付状态，默认为0：未支付 */
    private Integer payStatus;
    /** 创建时间 */
    @JsonSerialize(using = Date2Long.class)  // 让毫秒变成秒
    private Date createTime;
    /** 更新时间 */
    @JsonSerialize(using = Date2Long.class)
    private Date updateTime;
    /** 订单商品列表 */
    List<OrderDetailPO> orderDetailPOList;
}
