package cn.xiangxu.demo.sell_demo.entity.entityPO;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author 王杨帅
 * @create 2018-04-21 13:30
 * @desc 商品类型实体类
 **/
@Table(name = "product_category")
@Data
@Entity
@DynamicUpdate
public class ProductCategoryPO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Integer categoryId;
    /**
     * 类目名字
     */
    @Column(name = "category_name")
    private String categoryName;
    /**
     * 类目编号
     */
    @Column(name = "category_type")
    private Integer categoryType;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    public ProductCategoryPO() {
    }

    public ProductCategoryPO(String categoryName, Integer categoryType) {
        this.categoryName = categoryName;
        this.categoryType = categoryType;
    }
}

