package cn.xiangxu.demo.sell_demo.entity.entityVO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import javax.persistence.Column;
import java.math.BigDecimal;

/**
 * @author 王杨帅
 * @create 2018-04-22 14:47
 * @desc 商品信息响应实体
 **/
@Data
public class ProductInfoVO {

    /**
     * 商品ID
     */
    @JsonProperty("id")
    private String productId;
    /**
     * 商品名称
     */
    @JsonProperty("name")
    private String productName;
    /**
     * 单价
     */
    @JsonProperty("price")
    private BigDecimal productPrice;
    /**
     * 描述
     */
    @JsonPropertyOrder("description")
    private String productDescription;
    /**
     * 小图
     */
    @JsonProperty("icon")
    private String productIcon;
}

