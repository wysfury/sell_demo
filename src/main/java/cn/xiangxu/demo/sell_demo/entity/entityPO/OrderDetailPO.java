package cn.xiangxu.demo.sell_demo.entity.entityPO;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 王杨帅
 * @create 2018-04-21 13:35
 * @desc 订单详情实体类
 **/
@Table(name = "order_detail")
@Data
@Entity
@DynamicUpdate
public class OrderDetailPO {

    /**
     * 订单详情ID
     */
    @Id
    @Column(name = "detail_id")
    private String detailId;
    /**
     * 订单ID
     */
    @Column(name = "order_id")
    private String orderId;
    /**
     * 产品ID
     */
    @Column(name = "product_id")
    private String productId;
    /**
     * 商品名称
     */
    @Column(name = "product_name")
    private String productName;
    /**
     * 当前价格,单位分
     */
    @Column(name = "product_price")
    private BigDecimal productPrice;
    /**
     * 数量
     */
    @Column(name = "product_quantity")
    private Integer productQuantity;
    /**
     * 小图
     */
    @Column(name = "product_icon")
    private String productIcon;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;
}

