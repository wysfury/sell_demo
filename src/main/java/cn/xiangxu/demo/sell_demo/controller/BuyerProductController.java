package cn.xiangxu.demo.sell_demo.controller;

import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductCategoryPO;
import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductInfoPO;
import cn.xiangxu.demo.sell_demo.entity.entityVO.ProductInfoVO;
import cn.xiangxu.demo.sell_demo.entity.entityVO.ProductTypeVO;
import cn.xiangxu.demo.sell_demo.entity.entityVO.ResultVO;
import cn.xiangxu.demo.sell_demo.enums.ProductStatusEnum;
import cn.xiangxu.demo.sell_demo.service.ProductCategoryService;
import cn.xiangxu.demo.sell_demo.service.ProductInfoService;
import cn.xiangxu.demo.sell_demo.util.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 王杨帅
 * @create 2018-04-22 16:27
 * @desc 买家端控制层
 **/
@RestController
@RequestMapping("/buyer")
@Slf4j
public class BuyerProductController {

    private final String className = getClass().getName();

    @Autowired
    private ProductInfoService productInfoService;

    @Autowired
    private ProductCategoryService productCategoryService;

    @GetMapping(value = "/product/list")
    public ResultVO productList() {
        // 01 获取在售的商品信息列表
        List<ProductInfoPO> productInfoPOList = productInfoService.findByProductStatus(ProductStatusEnum.ON_SALE.getCode());
        log.info("===/" + className + "/test===获取到的商品信息列表为：{}", productInfoPOList);

        // 02 从获取到的商品信息列表中获取所有商品类型并组成一个商品类型列表
        List<Integer> categoryList
                = productInfoPOList.stream()
                .map(e -> e.getCategoryType())
                .collect(Collectors.toList());
        log.info("===/" + className + "/test===获取到的商品类型列表为：{}", categoryList);

        // 03 根据商品类型列表查询商品类型信息
        List<ProductCategoryPO> productCategoryPOList = productCategoryService.findByCategoryTypeIn(categoryList);
        log.info("===/" + className + "/test===获取到的商品类型信息列表为：{}", productCategoryPOList );

        List<ProductTypeVO> productTypeVOList = new ArrayList<>(); // 用于存放商品类型列表（前端的）

        // 04 数据整合
        for (ProductCategoryPO productCategoryPO : productCategoryPOList) {
            ProductTypeVO productTypeVO = new ProductTypeVO(); // 创建临时商品类型（前端的）
            productTypeVO.setType(productCategoryPO.getCategoryType());
            productTypeVO.setName(productCategoryPO.getCategoryName());

            List<ProductInfoVO> productInfoVOList = new ArrayList<>(); // 用于存放啥商品信息列表（前端的）
            for (ProductInfoPO productInfoPO : productInfoPOList) {
                if (productInfoPO.getCategoryType().equals(productCategoryPO.getCategoryType())) {
                    ProductInfoVO productInfoVO = new ProductInfoVO();
                    BeanUtils.copyProperties(productInfoPO, productInfoVO);
                    productInfoVOList.add(productInfoVO);
                }
            }

            productTypeVO.setProductInfoVOList(productInfoVOList); // 将商品信息存放到商品类目列表中（前端的）

            productTypeVOList.add(productTypeVO); // 将商品类目存放到商品类目列表中（前端的）

        }

        return ResultUtil.success(productTypeVOList);
    }

}

