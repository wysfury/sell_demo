package cn.xiangxu.demo.sell_demo.controller;

import cn.xiangxu.demo.sell_demo.entity.entityDTO.OrderDTO;
import cn.xiangxu.demo.sell_demo.entity.entityForm.OrderForm;
import cn.xiangxu.demo.sell_demo.entity.entityVO.ResultVO;
import cn.xiangxu.demo.sell_demo.enums.ResultEnum;
import cn.xiangxu.demo.sell_demo.exceptions.SellException;
import cn.xiangxu.demo.sell_demo.service.BuyerService;
import cn.xiangxu.demo.sell_demo.service.OrderService;
import cn.xiangxu.demo.sell_demo.util.ResultUtil;
import cn.xiangxu.demo.sell_demo.util.converter.OrderForm2OrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/buyer/order")
@Slf4j
public class BuyerOrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private BuyerService buyerService;

    // 创建订单
    @PostMapping("/create")
    public ResultVO<Map<String, String>> create(@Valid OrderForm orderForm, BindingResult bindingResult
    ) {
        log.info("===/" + getClass().getName() + "/===前端获取到额参数为：{}", orderForm);
        // 01 前端参数判断
        if (bindingResult.hasErrors()) {
            log.error("===/" + getClass().getName() + "【创建订单】-前端传入的参数不正确-orderForm={}",orderForm);
            throw new SellException(ResultEnum.PARAM_ERROR.getCode(),
                    bindingResult.getFieldError().getDefaultMessage());
        }
        // 02 前端参数转换
        OrderDTO orderDTO = OrderForm2OrderDTO.convert(orderForm);
        if (CollectionUtils.isEmpty(orderDTO.getOrderDetailPOList())) {
            log.error("【创建订单】-购物车不能为空");
            throw new SellException(ResultEnum.ORDER_CAR_EMPTY);
        }

        // 03 创建订单
        OrderDTO createResult = orderService.create(orderDTO);
        Map<String, String> map = new HashMap<>();
        map.put("orderId", createResult.getOrderId());
        return ResultUtil.success(map);
    }

    // 订单列表
    @GetMapping("/list")
    public ResultVO<List<OrderDTO>> list(
            @RequestParam("openid") String openid,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size
    ) {
        log.info("===/" + getClass().getName() + "/===前端获取到额参数为：{}", openid);

        // 01 查看openId参数是否为空
        if (StringUtils.isEmpty(openid)) {
            log.error("===/" + getClass().getName() + "【查询订单列表】-openid为空");
            throw new SellException(ResultEnum.PARAM_ERROR);
        }

        // 02 根据参数封装分页对象
        PageRequest pageRequest = PageRequest.of(page, size);

        // 03 根据openId进行查询订单列表（分页查询）
        Page<OrderDTO> orderDTOPage = orderService.findList(openid, pageRequest);

        // 04 放回订单列表
        return ResultUtil.success(orderDTOPage.getContent());
    }

    // 订单详情
    @GetMapping("/detail")
    public ResultVO<OrderDTO> detail(
            @RequestParam("openid") String openid,
            @RequestParam("orderId") String orderId
    ) {
        // TODO: 参数非空判断

        // 01 根据订单ID去查询到订单
        // 02 比较查询到的订单信息中的openId和传过来的是否一致
        // 03 返回订单详情操作

        OrderDTO orderDTO = buyerService.findOrderOne(openid, orderId);
        return ResultUtil.success(orderDTO);
    }

    // 取消订单
    @PostMapping("/cancel")
    public ResultVO cancel(
            @RequestParam("openid") String openid,
            @RequestParam("orderId") String orderId
    ) {

//        TODO: 参数非空判断

        // 01 根据订单ID去查询到订单
        // 02 比较查询到的订单信息中的openId和传过来的是否一致
        // 03 取消订单操作
        buyerService.cancelOrderOne(openid, orderId);
        return ResultUtil.success();
    }
}
