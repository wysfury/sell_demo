package cn.xiangxu.demo.sell_demo.controller;

import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductCategoryPO;
import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductInfoPO;
import cn.xiangxu.demo.sell_demo.entity.entityVO.ProductInfoVO;
import cn.xiangxu.demo.sell_demo.entity.entityVO.ProductTypeVO;
import cn.xiangxu.demo.sell_demo.entity.entityVO.ResultVO;
import cn.xiangxu.demo.sell_demo.enums.ProductStatusEnum;
import cn.xiangxu.demo.sell_demo.service.ProductCategoryService;
import cn.xiangxu.demo.sell_demo.service.ProductInfoService;
import cn.xiangxu.demo.sell_demo.util.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 王杨帅
 * @create 2018-04-21 11:04
 * @desc 测试控制层
 **/
@RestController
@RequestMapping(value = "/test")
@Slf4j
public class TestController {

    private final String className = getClass().getName();

    @Autowired
    private ProductInfoService productInfoService;

    @Autowired
    private ProductCategoryService productCategoryService;

    @GetMapping(value = "/connect")
    public String connect() {
        String result = "连接测试_fury";
        log.info("===/" + getClass().getName() + "/connect==={}", result);
        return result;
    }

    @GetMapping(value = "/structure")
    public ResultVO structure() {
        return ResultUtil.success("响应结构测试");
    }

    @GetMapping(value = "/test")
    public ResultVO test() {

//        // 01 获取在售的商品信息列表
//        List<ProductInfoPO> productInfoPOList = productInfoService.findByProductStatus(ProductStatusEnum.ON_SALE.getCode());
//        log.info("===/" + className + "/test===获取到的商品信息列表为：{}", productInfoPOList);
//
//        // 02 从获取到的商品信息列表中获取所有商品类型并组成一个商品类型列表
//        List<Integer> categoryList
//                        = productInfoPOList.stream()
//                            .map(e -> e.getCategoryType())
//                            .collect(Collectors.toList());
//        log.info("===/" + className + "/test===获取到的商品类型列表为：{}", categoryList);
//
//        // 03 根据商品类型列表查询商品类型信息
//        List<ProductCategoryPO> productCategoryPOList = productCategoryService.findByCategoryTypeIn(categoryList);
//        log.info("===/" + className + "/test===获取到的商品类型信息列表为：{}", productCategoryPOList );
//
//        List<ProductTypeVO> productTypeVOList = new ArrayList<>();
//
//        // 04 数据整合
//        for (ProductCategoryPO productCategoryPO : productCategoryPOList) {
//            ProductTypeVO productTypeVO = new ProductTypeVO();
//            productTypeVO.setType(productCategoryPO.getCategoryType());
//            productTypeVO.setName(productCategoryPO.getCategoryName());
//
//            List<ProductInfoVO> productInfoVOList = new ArrayList<>();
//            for (ProductInfoPO productInfoPO : productInfoPOList) {
//                if (productInfoPO.getCategoryType().equals(productCategoryPO.getCategoryType())) {
//                    ProductInfoVO productInfoVO = new ProductInfoVO();
//                    BeanUtils.copyProperties(productInfoPO, productInfoVO);
//                    productInfoVOList.add(productInfoVO);
//                }
//            }
//
//            productTypeVO.setProductInfoVOList(productInfoVOList);
//
//            productTypeVOList.add(productTypeVO);
//
//        }
//
//        return ResultUtil.success(productTypeVOList);
        return ResultUtil.success();
    }

}

