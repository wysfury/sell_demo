package cn.xiangxu.demo.sell_demo.service.impl;

import cn.xiangxu.demo.sell_demo.dao.ProductInfoDao;
import cn.xiangxu.demo.sell_demo.entity.entityDTO.CarDTO;
import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductInfoPO;
import cn.xiangxu.demo.sell_demo.enums.ResultEnum;
import cn.xiangxu.demo.sell_demo.exceptions.SellException;
import cn.xiangxu.demo.sell_demo.service.ProductInfoService;
import com.fasterxml.jackson.databind.util.BeanUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 商品信息服务层实现类
 * @author 王杨帅
 * @since 2018-04-21
 */
@Service
public class ProductInfoServiceImpl implements ProductInfoService {

    @Autowired
    private ProductInfoDao productInfoDao;

    @Override
    @Transactional
    public ProductInfoPO create(ProductInfoPO productInfoPO) {
        // 01 根据商品ID查询
        Optional<ProductInfoPO> old = productInfoDao.findById(productInfoPO.getProductId());

        // 02 如果查询到数据就抛出异常：商品信息已经存在
        if (old.isPresent()) {
            throw new SellException(ResultEnum.PRODUCT_ID_IS_EXIST);
        }

        // 03 进行更新操作
        ProductInfoPO result = productInfoDao.save(productInfoPO);

        // 04 返回更新结果
        return result;
    }

    @Override
    @Transactional
    public ProductInfoPO update(ProductInfoPO productInfoPO) {
        // 01 根据ID查询数据
        Optional<ProductInfoPO> old = productInfoDao.findById(productInfoPO.getProductId());

        // 02 如果查询不到数据就抛出异常：商品信息不存在
        if (!old.isPresent()) {
            throw new SellException(ResultEnum.PRODUCT_INFO_IS_NULL);
        }

        // 03 更新数据
        ProductInfoPO newData = old.get();
        productInfoPO.setCreateTime(newData.getCreateTime());
        BeanUtils.copyProperties(productInfoPO, newData);
        ProductInfoPO result = productInfoDao.save(newData);

        // 04 返回更新结果
        return result;

    }

    @Override
    public ProductInfoPO findById(String productId) {
        // 01 根据ID查询数据
        Optional<ProductInfoPO> old = productInfoDao.findById(productId);

        // 02 如果查询不到数据就抛出异常：商品信息不存在
        if (!old.isPresent()) {
            throw new SellException(ResultEnum.PRODUCT_INFO_IS_NULL);
        }

        // 03 返回更新结果
        return old.get();
    }

    @Override
    public List<ProductInfoPO> findByProductStatus(Integer status) {
        // 01 根据状态进行查询
        List<ProductInfoPO> result = productInfoDao.findByProductStatus(status);

        // 02 返回查询结果
        return result;
    }

    @Override
    @Transactional
    public void increseStock(List<CarDTO> carDTOS) {
        for (CarDTO carDTO : carDTOS) {
            // 01 根据商品ID获取商品信息,如果为空就抛出异常
            Optional<ProductInfoPO> productInfoPOOptional = productInfoDao.findById(carDTO.getProductId());
            if (!productInfoPOOptional.isPresent()) {
                throw new SellException(ResultEnum.PRODUCT_INFO_NOT_EXIST);
            }
            ProductInfoPO oldProductInfo = productInfoPOOptional.get();

            // 02 库存量
            Integer stock = oldProductInfo.getProductStock() + carDTO.getProductQuantity();

            // 03 重设库存量
            oldProductInfo.setProductStock(stock);

            // 04 进行扣库存操作
            productInfoDao.save(oldProductInfo);

        }
    }

    @Override
    @Transactional
    public void decreaseStock(List<CarDTO> carDTOS) {
        for (CarDTO carDTO : carDTOS) {
            // 01 根据商品ID获取商品信息,如果为空就抛出异常
            Optional<ProductInfoPO> productInfoPOOptional = productInfoDao.findById(carDTO.getProductId());
            if (!productInfoPOOptional.isPresent()) {
                throw new SellException(ResultEnum.PRODUCT_INFO_NOT_EXIST);
            }
            ProductInfoPO oldProductInfo = productInfoPOOptional.get();

            // 02 库存量
            Integer stock = oldProductInfo.getProductStock() - carDTO.getProductQuantity();

            // 03 判断库存量
            if (stock < 0) {
                throw new SellException(ResultEnum.PRODUCT_STOCK_INSUFFICIENT);
            }

            // 04 重设库存量
            oldProductInfo.setProductStock(stock);

            // 05 进行扣库存操作
            productInfoDao.save(oldProductInfo);

        }

    }
}
