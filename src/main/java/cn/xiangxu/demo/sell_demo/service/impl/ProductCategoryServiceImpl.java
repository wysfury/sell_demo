package cn.xiangxu.demo.sell_demo.service.impl;


import cn.xiangxu.demo.sell_demo.dao.ProductCategoryDao;
import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductCategoryPO;
import cn.xiangxu.demo.sell_demo.enums.ResultEnum;
import cn.xiangxu.demo.sell_demo.exceptions.SellException;
import cn.xiangxu.demo.sell_demo.service.ProductCategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 商品类目服务层实现类
 */
@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Autowired
    private ProductCategoryDao productCategoryDao;

    @Override
    public ProductCategoryPO create(ProductCategoryPO productCategoryPO) {
        // 01 判断商品类目类型是否存在;
        List<ProductCategoryPO> result = productCategoryDao.findByCategoryTypeIn(Arrays.asList(productCategoryPO.getCategoryType()));

        // 02 如果存在就抛出异常：商品类目类型ID已经存在
        if (result.size() != 0) {
            throw new SellException(ResultEnum.PRODUCT_CATEGORY_TYPE_IS_EXIST);
        }

        // 03 进行插入或更新操作
        ProductCategoryPO createResult = productCategoryDao.save(productCategoryPO);

        // 04 返回新增对象
        return createResult;
    }

    @Override
    public ProductCategoryPO update(ProductCategoryPO productCategoryPO) {
        // 01 判断商品类目ID是否存在;如果不存在抛出异常：商品类目不存在
        ProductCategoryPO result = findById(productCategoryPO.getCategoryId());

        // 02 进行更新操作
        productCategoryPO.setCreateTime(result.getCreateTime()); // 获取创建时间
//        productCategoryPO.setUpdateTime(result.getCreateTime());
        BeanUtils.copyProperties(productCategoryPO, result); // 将更新信息复制给result,此时result的更新时间为null；只有为null时数据库才会自动更新时间，否者会按照给定的值进行更新
        ProductCategoryPO updateResult = productCategoryDao.save(result);

        // 03 返回更新结果
        return updateResult;
    }

    @Override
    public ProductCategoryPO findByCategoryType(Integer categoryType) {
        return null;
    }

    @Override
    public ProductCategoryPO findById(Integer id) {
        // 01 调用持久层方法进行查询
        Optional<ProductCategoryPO> result =  productCategoryDao.findById(id);

        // 02 判断查询结果是否为空，如果为空就抛出异常：商品类目信息不存在
        if (!result.isPresent()) {
            throw new SellException(ResultEnum.PRODUCT_CATEGORY_IS_NULL);
        }

        // 03 返回查询结果
        return result.get();
    }

    @Override
    public List<ProductCategoryPO> findAll() {
        return productCategoryDao.findAll();
    }

    @Override
    public List<ProductCategoryPO> findByCategoryTypeIn(List<Integer> categoryTypeList) {

        // 01 调用持久层进行查询
        List<ProductCategoryPO> result = productCategoryDao.findByCategoryTypeIn(categoryTypeList);

        // 02 判断查询结果，如果结果为空，就抛出异常：商品类目信息不存在
        if (result.size() == 0) {
            throw new SellException(ResultEnum.PRODUCT_CATEGORY_IS_NULL);
        }

        // 03 返回查询结果
        return result;
    }
}
