package cn.xiangxu.demo.sell_demo.service.impl;

import cn.xiangxu.demo.sell_demo.service.SellerInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 卖家信息表 服务实现类
 * </p>
 *
 * @author 王杨帅
 * @since 2018-04-21
 */
@Service
public class SellerInfoServiceImpl implements SellerInfoService {
	
}
