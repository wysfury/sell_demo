package cn.xiangxu.demo.sell_demo.service;

import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductCategoryPO;

import java.util.List;

/**
 * 商品类目服务层接口
 * @author 王杨帅
 * @since 2018-04-21
 */

public interface ProductCategoryService {
    /** 新增商品类目信息 */
	ProductCategoryPO create(ProductCategoryPO productCategoryPO);

	/** 更新商品类目信息 */
    ProductCategoryPO update(ProductCategoryPO productCategoryPO);

	/** 根据商品类目类型ID查询商品类目信息 */
	ProductCategoryPO findByCategoryType(Integer categoryType);

	/** 根据记录ID查询 */
	ProductCategoryPO findById(Integer id);

	/** 查询所有商品类目信息 */
	List<ProductCategoryPO> findAll();

	/** 根据商品类目类型ID列表查询商品类目信息 */
    List<ProductCategoryPO> findByCategoryTypeIn(List<Integer> categoryTypeList);
}
