package cn.xiangxu.demo.sell_demo.service;


import cn.xiangxu.demo.sell_demo.entity.entityDTO.CarDTO;
import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductInfoPO;

import java.util.List;

/**
 * 商品信息服务层接口
 * @author 王杨帅
 * @since 2018-04-21
 */
public interface ProductInfoService {
    /** 新增商品信息 */
	ProductInfoPO create(ProductInfoPO productInfoPO);

	/** 更新商品信息 */
	ProductInfoPO update(ProductInfoPO productInfoPO);

	/** 根据商品ID获取数据 */
	ProductInfoPO findById(String productId);

	/** 根据商品状态获取数据 */
	List<ProductInfoPO> findByProductStatus(Integer status);

	//  加库存
	void increseStock(List<CarDTO> carDTOS);

	// 减库存
	void decreaseStock(List<CarDTO> carDTOS);
}
