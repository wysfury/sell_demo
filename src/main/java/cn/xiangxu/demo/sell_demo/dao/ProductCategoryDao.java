package cn.xiangxu.demo.sell_demo.dao;

import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductCategoryPO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 王杨帅
 * @create 2018-04-21 13:43
 * @desc 商品类目持久层接口
 **/
public interface ProductCategoryDao extends JpaRepository<ProductCategoryPO, Integer> {

    /** 根据类目列表查询商品类目信息 */
    List<ProductCategoryPO> findByCategoryTypeIn(List<Integer> categoryTypeList);
}
