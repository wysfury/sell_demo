package cn.xiangxu.demo.sell_demo.dao;

import cn.xiangxu.demo.sell_demo.entity.entityPO.OrderDetailPO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 王杨帅
 * @create 2018-04-21 13:41
 * @desc 订单详情持久层接口
 **/
public interface OrderDetailDao extends JpaRepository<OrderDetailPO, String> {

    /** 根据订单ID查询订单详情信息 */
    List<OrderDetailPO> findByOrOrderId(String orderId);
}
