package cn.xiangxu.demo.sell_demo.dao;

import cn.xiangxu.demo.sell_demo.entity.entityPO.OrderMasterPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 王杨帅
 * @create 2018-04-21 13:42
 * @desc 订单持久层接口
 **/
public interface OrderMasterDao extends JpaRepository<OrderMasterPO, String> {

    /**
     * 根据买家的微信OpenId查询订单信息
     * @param openId
     * @return
     */
    List<OrderMasterPO> findByBuyerOpenid(String openId);

    /**
     * 根据买家的微信OpenId查询订单信息（分页功能）
     * @param openId
     * @param pageable
     * @return
     */
    Page<OrderMasterPO> findByBuyerOpenid(String openId, Pageable pageable);
}
