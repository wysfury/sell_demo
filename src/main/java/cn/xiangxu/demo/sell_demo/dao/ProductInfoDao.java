package cn.xiangxu.demo.sell_demo.dao;

import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductInfoPO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 王杨帅
 * @create 2018-04-21 13:43
 * @desc 商品信息持久层接口
 **/
public interface ProductInfoDao extends JpaRepository<ProductInfoPO, String> {
    /** 根据商品状态查询 */
    List<ProductInfoPO> findByProductStatus(Integer status);
}
