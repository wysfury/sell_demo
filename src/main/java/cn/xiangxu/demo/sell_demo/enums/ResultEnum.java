package cn.xiangxu.demo.sell_demo.enums;

import lombok.Getter;

/**
 * @author 王杨帅
 * @create 2018-04-21 16:07
 * @desc 全局异常枚举
 **/
@Getter
public enum ResultEnum {
    PRODUCT_CATEGORY_IS_NULL(101, "商品类目信息为空"),
    PRODUCT_CATEGORY_TYPE_IS_EXIST(102, "商品类目类型已经存在"),

    PRODUCT_INFO_IS_NULL(201, "商品信息为空"),
    PRODUCT_ID_IS_EXIST(202, "商品ID已经存在"),
    PRODUCT_INFO_NOT_EXIST(203, "商品信息不存在"),
    PRODUCT_STOCK_INSUFFICIENT(204, "商品库存量不足"),

    ORDER_INFO_IS_NULL(301, "订单信息为空"),
    ORDER_DETAIL_INFO_IS_NULL(302, "订单详情为空"),
    ORDER_STATUS_ERROR(303, "订单状态不正确"),
    ORDER_UPDATE_STATUS_ERROR(304, "更新订单状态失败"),
    ORDER_PRODUCT_LIST_IS_NULL(305, "该订单无任何商品信息"),
    ORDER_PAY_STATUS_ERROR(306, "订单支付状态不正确"),
    ORDER_UPDATE_PAY_SATTUS_ERROR(307, "更新订单支付状态失败"),
    ORDER_NOT_EXIST(308, "订单不存在"),
    ORDER_OWNER_ERROR(309, "订单所有者错误"),
    ORDER_CAR_EMPTY(310, "订单对应的购物车为空"),

    PARAM_ERROR(401, "请求参数错误")
    ;
    private Integer code;
    private String messge;

    ResultEnum(Integer code, String messge) {
        this.code = code;
        this.messge = messge;
    }
}
