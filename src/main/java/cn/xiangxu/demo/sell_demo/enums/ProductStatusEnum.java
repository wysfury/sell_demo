package cn.xiangxu.demo.sell_demo.enums;

import lombok.Getter;

/**
 * @author 王杨帅
 * @create 2018-04-21 21:19
 * @desc 商品状态枚举
 **/
@Getter
public enum ProductStatusEnum {
    ON_SALE(0, "在售"),
    SOLD_OUT(1, "下架")
    ;
    private Integer code;
    private String message;

    ProductStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    ProductStatusEnum() {

    }
}
