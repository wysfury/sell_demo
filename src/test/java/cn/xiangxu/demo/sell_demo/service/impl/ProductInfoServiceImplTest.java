package cn.xiangxu.demo.sell_demo.service.impl;

import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductInfoPO;
import cn.xiangxu.demo.sell_demo.enums.ProductStatusEnum;
import cn.xiangxu.demo.sell_demo.service.ProductInfoService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductInfoServiceImplTest {

    private String className = getClass().getName();

    @Autowired
    private ProductInfoService productInfoService;

    @Test
    public void create() throws Exception {
        // 01 构造商品信息对象
        ProductInfoPO productInfoPO = new ProductInfoPO();
        productInfoPO.setProductId("20180004");
        productInfoPO.setProductName("回锅肉");
        productInfoPO.setProductPrice(new BigDecimal(33));
        productInfoPO.setProductStock(100);
        productInfoPO.setProductDescription("香辣可口");
        productInfoPO.setProductIcon("http://sasd.com");
        productInfoPO.setCategoryType(3);

        // 02 新增操作
        ProductInfoPO result = productInfoService.create(productInfoPO);

        // 03 打印输出数据
        log.info("===/" + className + "/create===新增结果为：{}", result);

    }

    @Test
    public void update() throws Exception {
        // 01 构造商品信息
        ProductInfoPO productInfoPO = new ProductInfoPO();
        productInfoPO.setProductId("20180004");
        productInfoPO.setProductName("回锅肉——修改");
        productInfoPO.setProductPrice(new BigDecimal(33));
        productInfoPO.setProductStock(100);
        productInfoPO.setProductDescription("香辣可口");
        productInfoPO.setProductIcon("http://sasd.com");
        productInfoPO.setCategoryType(3);

        // 02 更新数据
        ProductInfoPO result = productInfoService.update(productInfoPO);

        // 03 打印更新数据
        log.info("===/" + className + "/update===更新后的结果为：{}", result);

    }

    @Test
    public void findById() throws Exception {
        // 01 模拟一个商品ID
        String productId = "20180004";

        // 02 根据商品ID获取数据
        ProductInfoPO result = productInfoService.findById(productId);

        // 03 打印获取到的商品信息
        log.info("===/" + className + "/findById==={}", result);
    }

    @Test
    public void findByProductStatus() throws Exception {
        // 01 根据商品状态查询
        List<ProductInfoPO> result = productInfoService.findByProductStatus(ProductStatusEnum.ON_SALE.getCode());

        // 02 打印获取到的数据
        log.info("===/" + className + "/findByProductStatus===根据状态查询到的数据为：{}", result);

    }

}