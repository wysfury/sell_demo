package cn.xiangxu.demo.sell_demo.service.impl;

import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductCategoryPO;
import cn.xiangxu.demo.sell_demo.service.ProductCategoryService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductCategoryServiceImplTest {

    @Autowired
    private ProductCategoryService productCategoryService;

    @Test
    public void create() throws Exception {
        ProductCategoryPO productCategoryPO = new ProductCategoryPO("罐头类食品", 5);
        ProductCategoryPO result = productCategoryService.create(productCategoryPO);
        log.info("===/" + getClass().getName() + "/create===新增后返回结果为：{}", result);
        Assert.assertNotEquals(null, result);
    }

    @Test
    public void update() throws Exception {
        ProductCategoryPO productCategoryPO = new ProductCategoryPO("罐头类食品_修改02", 5);
        productCategoryPO.setCategoryId(16);
        ProductCategoryPO result = productCategoryService.update(productCategoryPO);
        log.info("===/" + getClass().getName() + "/update===更新后返回结果为：{}", result);
        Assert.assertNotEquals(null, result);
    }

    @Test
    public void findByCategoryType() throws Exception {
    }

    @Test
    public void findById() throws Exception {
        Integer id = 16;
        ProductCategoryPO result = productCategoryService.findById(id);
        log.info("===/" + getClass().getName() + "/findById===根据类目ID查询得到的结果为：{}", result);
        Assert.assertNotEquals(null, result);
    }

    @Test
    public void findAll() throws Exception {
        List<ProductCategoryPO> result = productCategoryService.findAll();
        log.info("===/" + getClass().getName() + "/findAll===根据类目ID查询得到的结果为：{}", result);
        Assert.assertNotEquals(0, result.size());
    }

    @Test
    public void findByCategoryTypeIn() throws Exception {
        List<ProductCategoryPO> result = productCategoryService.findByCategoryTypeIn(Arrays.asList(0,1,2));
        log.info("===/" + getClass().getName() + "/findByCategoryTypeIn===根据类目ID查询得到的结果为：{}", result);
        Assert.assertNotEquals(0, result.size());
    }

}