package cn.xiangxu.demo.sell_demo.service.impl;

import cn.xiangxu.demo.sell_demo.entity.entityDTO.OrderDTO;
import cn.xiangxu.demo.sell_demo.entity.entityPO.OrderDetailPO;
import cn.xiangxu.demo.sell_demo.enums.OrderStatusEnum;
import cn.xiangxu.demo.sell_demo.enums.PayStatusEnum;
import cn.xiangxu.demo.sell_demo.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderServiceImplTest {

    private final String className = getClass().getName();

    @Autowired
    private OrderService orderService;

    private final static String OPEN_ID = "2012114018";
    private final static String ORDER_ID = "1524449633120391319";

    @Test
    public void create() throws Exception {
        // 01 构建前端传过来的订单信息
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName("王正军");
        orderDTO.setBuyerPhone("16620064649");
        orderDTO.setBuyerAddress("重庆市沙坪坝区三峡广场");
        orderDTO.setBuyerOpenid("2012114018");

        // 购物车（其实就是订单详情的浓缩版本）
        // 商品列表
        List<OrderDetailPO> orderDetails = new ArrayList<>();
        // 商品1
        OrderDetailPO orderDetail01 = new OrderDetailPO();
        orderDetail01.setProductId("20180003");
        orderDetail01.setProductQuantity(10);
        orderDetails.add(orderDetail01);
        // 商品2
        OrderDetailPO orderDetail02 = new OrderDetailPO();
        orderDetail02.setProductId("20180002");
        orderDetail02.setProductQuantity(10);
        orderDetails.add(orderDetail02);

        orderDTO.setOrderDetailPOList(orderDetails);

        // 02 调用订单服务层新增订单
        OrderDTO result = orderService.create(orderDTO);

        // 03 打印新增订单后返回的信息
        log.info("===/" +className + "/create===新增订单后获取到的数据为：{}", result);
    }

    @Test
    public void findByOrderId() throws Exception {
        // 01 模拟订单ID
        String orderId = "1524449742298312821";

        // 02 根据订单ID查询订单信息
        OrderDTO result = orderService.findByOrderId(orderId);

        // 03 打印获取到的数据
        log.info("===/" + className + "/findByOrderId===获取到的订单信息为：{}", result);
    }

    @Test
    public void findList() throws Exception {
        PageRequest pageRequest = PageRequest.of(0, 3);
        Page<OrderDTO> orderDTOPage = orderService.findList(OPEN_ID, pageRequest);
        log.info("===/" + className + "/findList===获取到的订单信息为01：{}", orderDTOPage.getContent());
        log.info("===/" + className + "/findList===获取到的订单信息为02：{}", orderDTOPage.getPageable());
        log.info("===/" + className + "/findList===获取到的订单信息为03：{}", orderDTOPage.getTotalElements());
        log.info("===/" + className + "/findList===获取到的订单信息为04：{}", orderDTOPage.getTotalPages());
    }

    @Test
    public void cancel() throws Exception {
        // 01 构建前端传过来的订单信息
        OrderDTO orderDTO = orderService.findByOrderId("1524449742298312821");

        // 02 取消订单操作
        OrderDTO result = orderService.cancel(orderDTO);

        // 03 打印取消结果
        log.info("===/" + className + "/cancel===取消订单后的结果为：{}", result);

    }

    @Test
    public void finish() throws Exception {
        OrderDTO orderDTO = orderService.findByOrderId(ORDER_ID);
        log.info("===={}", orderDTO);
        OrderDTO result = orderService.finish(orderDTO);
        Assert.assertEquals(OrderStatusEnum.FINISHED.getCode(), result.getOrderStatus());
    }

    @Test
    public void paid() throws Exception {
        OrderDTO orderDTO = orderService.findByOrderId(ORDER_ID);
        OrderDTO result = orderService.paid(orderDTO);
        Assert.assertEquals(PayStatusEnum.SUCCESS.getCode(), result.getPayStatus());
    }

}