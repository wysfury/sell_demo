package cn.xiangxu.demo.sell_demo.dao;

import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductCategoryPO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductCategoryDaoTest {
    @Autowired
    private ProductCategoryDao productCategoryDao;

    /**
     * 新增商品类目
     * 技巧01：不用指定记录ID、创建时间、更新时间，因为他们都是由数据库自动生成
     */
    @Test
    public void create() {
        ProductCategoryPO productCategoryPO = new ProductCategoryPO("腌菜食品", 3);
        ProductCategoryPO result = productCategoryDao.save(productCategoryPO);
        log.info("===/" + getClass().getName() + "/===新增后返回的结果为：{}", result);
        Assert.assertNotEquals(null, result);
    }

    /**
     * 根据商品类目ID查询对应的商品类目信息
     */
    @Test
    public void findById() {
        Integer id = 7;
        Optional<ProductCategoryPO> productCategoryPOOptional = productCategoryDao.findById(id);
        log.info("/===" + getClass().getName() + "/findById===根据ID获取到结果为：{}", productCategoryPOOptional.get());
        Assert.assertNotEquals(null, productCategoryPOOptional.get());
    }

    /**
     * 更新商品类目
     * 技巧01：必须提供记录ID，因为JPA的save方法时根据记录ID进行更新操作的，如果数据库中存在该记录ID就会进行更新操作，如果不存在就会进行
     * 新增操作；但是如果数据库表设定了主键自增，那么这里即使是新增操作也不会使用传入的记录ID
     */
    @Test
    public void update() {
        Optional<ProductCategoryPO> productCategoryPOOptional = productCategoryDao.findById(7);
        Assert.assertNotEquals(null, productCategoryPOOptional.get());
        ProductCategoryPO old = productCategoryPOOptional.get();
        log.info("====原始值：{}", old.toString());
        old.setCategoryName("垃圾商品");
        old.setCreateTime(null);
        log.info("===修改值：{}", old.toString());
        ProductCategoryPO result = productCategoryDao.save(old);
        log.info("===更新结果值：{}", result.toString());
        Assert.assertNotEquals(null, result);
    }

    /**
     * 根据类目列表查询商品类目信息
     */
    @Test
    public void findByCategoryTypeIn() {
        List<Integer> typeList = Arrays.asList(0,1);
        List<ProductCategoryPO> productCategoryPOList = productCategoryDao.findByCategoryTypeIn(typeList);
        log.info("===/" + getClass().getName() + "/===获取到的数据为：{}", productCategoryPOList.toString());
        Assert.assertNotEquals(0, productCategoryPOList.size());
    }
}