package cn.xiangxu.demo.sell_demo.dao;

import cn.xiangxu.demo.sell_demo.entity.entityPO.OrderMasterPO;
import cn.xiangxu.demo.sell_demo.enums.OrderStatusEnum;
import cn.xiangxu.demo.sell_demo.enums.PayStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderMasterDaoTest {

    private final String className = getClass().getName();

    @Autowired
    private OrderMasterDao orderMasterDao;

    @Test
    public void create() {

        // 01 构建订单对象
        OrderMasterPO orderMasterPO = new OrderMasterPO();
        orderMasterPO.setOrderId("20180001");
        orderMasterPO.setBuyerName("王杨帅");
        orderMasterPO.setBuyerPhone("13272885616");
        orderMasterPO.setBuyerAddress("广东工业大学");
        orderMasterPO.setBuyerOpenid("123456");
        orderMasterPO.setOrderAmount(new BigDecimal(333));
        orderMasterPO.setOrderStatus(OrderStatusEnum.NEW.getCode());
        orderMasterPO.setPayStatus(PayStatusEnum.WAIT.getCode());

        // 02 调用持久层对象进行新增操作
        OrderMasterPO result = orderMasterDao.save(orderMasterPO);

        // 03 打印新增结果数据
        log.info("===/" + className + "/create===更新后返回的结果为：{}", result);
        Assert.assertNotEquals(null, result);

    }

    @Test
    public void findByBuyerOpenid() throws Exception {
        // 01 模拟一个微信openId
        String openId = "123456";

        // 02 根据微信OpenId查询数据
        List<OrderMasterPO> result = orderMasterDao.findByBuyerOpenid(openId);

        // 03 打印查询到的数据
        log.info("===/" + className + "/findByBuyerOpenid===根据微信openId查询到的数据为：{}", result);
    }

}