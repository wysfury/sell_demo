package cn.xiangxu.demo.sell_demo.dao;

import cn.xiangxu.demo.sell_demo.entity.entityPO.OrderDetailPO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderDetailDaoTest {

    private final String className = getClass().getName();

    @Autowired
    private OrderDetailDao orderDetailDao;

    @Test
    public void create() {
        // 01 模拟一个订单详情实例
        OrderDetailPO orderDetailPO = new OrderDetailPO();
        orderDetailPO.setDetailId("2");
        orderDetailPO.setOrderId("20180001");
        orderDetailPO.setProductId("20180001");
        orderDetailPO.setProductName("皮蛋粥");
        orderDetailPO.setProductPrice(new BigDecimal(3.5));
        orderDetailPO.setProductQuantity(3);
        orderDetailPO.setProductIcon("http://xxasd.com");

        // 02 执行新增操作
        OrderDetailPO result = orderDetailDao.save(orderDetailPO);

        // 03 返回结果
        log.info("===/" + className + "/create===新增后的返回数据为：{}", result);
        Assert.assertNotEquals(null, result);
    }

    @Test
    public void findByOrOrderId() throws Exception {
        // 模拟一个订单ID
        String orderId = "20180001";

        // 根据订单ID进行查询
        List<OrderDetailPO> result = orderDetailDao.findByOrOrderId(orderId);

        // 记录查询结果
        log.info("===/" + className + "/findByOrderId===根据订单ID查询到的数据为：{}", result);
        Assert.assertNotEquals(0, result.size());
    }

}