package cn.xiangxu.demo.sell_demo.dao;

import cn.xiangxu.demo.sell_demo.entity.entityPO.ProductInfoPO;
import cn.xiangxu.demo.sell_demo.enums.ProductStatusEnum;
import cn.xiangxu.demo.sell_demo.enums.ResultEnum;
import cn.xiangxu.demo.sell_demo.exceptions.SellException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.text.html.Option;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductInfoDaoTest {

    private String className = getClass().getName();

    @Autowired
    private ProductInfoDao productInfoDao;

    @Test
    public void create() {
        ProductInfoPO productInfoPO = new ProductInfoPO();
        productInfoPO.setProductId("20180003");
        productInfoPO.setProductName("纸上烤鱼");
        productInfoPO.setProductPrice(new BigDecimal(88));
        productInfoPO.setProductStock(100);
        productInfoPO.setProductDescription("鲜香");
        productInfoPO.setProductIcon("http://sasd.com");
        productInfoPO.setCategoryType(4);
        ProductInfoPO result = productInfoDao.save(productInfoPO);
        log.info("===/" + className + "/create===新增商品信息后返回的结果为：{}", result);
        Assert.assertNotEquals(null, result);
    }

    @Test
    public void update() {
        // 01 创建修改对象
        ProductInfoPO productInfoPO = new ProductInfoPO();
        productInfoPO.setProductId("20180002");
        productInfoPO.setProductName("重庆小面");
        productInfoPO.setProductPrice(new BigDecimal(6));
        productInfoPO.setProductStock(9999);
        productInfoPO.setProductDescription("麻辣爽口");
        productInfoPO.setProductIcon("http://sasd.com");
        productInfoPO.setCategoryType(2);

        // 02 根据商品ID查询
        Optional<ProductInfoPO> old = productInfoDao.findById(productInfoPO.getProductId());

        // 03 如果不存在对应商品信息就抛出异常
        if (!old.isPresent()) {
            throw new SellException(ResultEnum.PRODUCT_INFO_IS_NULL);
        }

        // 04 更新数据封装
        ProductInfoPO newData = old.get();
        productInfoPO.setCreateTime(newData.getCreateTime());
        BeanUtils.copyProperties(productInfoPO, newData);

        // 05 更新操作
        ProductInfoPO result = productInfoDao.save(newData);

        // 06 打印更新结果
        log.info("===/" + className + "/update===更新后返回的结果为：{}", result);

    }

    @Test
    public void findById() {
        // 01 模拟一个商品ID
        String productId = "20180001";

        // 02 根据ID查询
        Optional<ProductInfoPO> result = productInfoDao.findById(productId);

        // 03 判断是否有对应的商品信息，如果没有抛出异常：商品信息为空
        if (!result.isPresent()) {
            throw new SellException(ResultEnum.PRODUCT_INFO_IS_NULL);
        }

        // 04 打印获取到的数据
        log.info("===/" + className + "/findById===根据ID获取到的商品信息为：{}", result.get());


    }

    @Test
    public void findByProductStatus() throws Exception {

        // 01 模拟一个商品状态
        Integer status = ProductStatusEnum.ON_SALE.getCode();

        // 02 根据商品状态查询
        List<ProductInfoPO> result = productInfoDao.findByProductStatus(status);

        // 03 打印获取到的数据
        log.info("===/" + className + "/findByProductStatus===根据商品状态获取到数据为：{}", result);
        Assert.assertNotEquals(0, result.size());
    }

}